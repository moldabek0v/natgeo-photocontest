from modeltranslation.translator import register, TranslationOptions
from main.models import Contest, Participant, ParticipantImages, Tags, FooterDetail, Footer, ContestMore


@register(Contest)
class ContestTranslation(TranslationOptions):
    fields = ('title', 'description')


@register(Participant)
class ParticipantTranslation(TranslationOptions):
    fields = ()


@register(ParticipantImages)
class ParticipantImagesTranslation(TranslationOptions):
    fields = ('title', 'description', )


@register(Tags)
class TagsTranslation(TranslationOptions):
    fields = ('tag',)


@register(Footer)
class FooterTranslation(TranslationOptions):
    fields = ('name',)


@register(FooterDetail)
class FooterDetailTranslation(TranslationOptions):
    fields = ('name',)


@register(ContestMore)
class ContestMoreTranslation(TranslationOptions):
    fields = ('description', 'title')
