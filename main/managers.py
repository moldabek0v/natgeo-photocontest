from django.db import models


class FavoriteNomineesManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .annotate(like_count=models.Count("contest"))
            .order_by("-like_count")
        )
