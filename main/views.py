import json
from datetime import datetime
from django.contrib.auth.decorators import user_passes_test
import requests
import xlwt
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic import TemplateView, DetailView, ListView
from django.views.generic.edit import FormView
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from django.shortcuts import redirect
from main.forms import ParticipantsForm, RegisterForm, LoginForm
from main.models import Contest, Participant, ParticipantUserLike, ParticipantImages, ContestMore, PhoneNumber
from natgeo import settings
from natgeo.settings import CAPTCHA_SECRET_KEY, CAPTCHA_SECRET_KEY_FOR_HTML
from .mixins import ContestParticipantMixin
from .utils import account_activation_token


class ContestListView(TemplateView):
    """
    Main Page
    """

    template_name = "contest/index.html"

    def get_context_data(self, **kwargs):
        context = super(ContestListView, self).get_context_data()
        context["contests"] = Contest.objects.all()
        return context


class ContestDetailView(DetailView):
    """
    Contest Detail Page witch display Contest.Info Contest.Description
    """

    model = Contest
    template_name = "contest/inside.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ContestDetailView, self).get_context_data(**kwargs)
        context["data"] = Contest.objects.get(id=self.kwargs.get("pk"))
        context["more"] = ContestMore.objects.filter(contest_id=self.kwargs.get("pk"))
        return context


class ContestNomineesView(ContestParticipantMixin):
    """
    List of Nominees witch can be order by popularity and newest
    """

    model = Contest
    template_name = "contest/nominees.html"
    participant_type = 1


class ParticipantView(DetailView):
    """
    Page display one of the work of participant
    """

    model = ParticipantImages
    template_name = "contest/nomineeInside.html"

    def get_context_data(self, **kwargs):
        context = super(ParticipantView, self).get_context_data(**kwargs)
        context["is_active_like_button"] = self.is_active_button()
        return context

    def is_active_button(self):
        if self.request.user.is_authenticated:
            user = self.request.user
            return not ParticipantUserLike.objects.filter(
                nominees_id=self.object.nominees.id, user_id=user.id
            ).exists()


class ParticipantFavoriteView(ContestParticipantMixin):
    """
    Display free-participants
    """

    model = Participant
    template_name = "contest/nominees.html"
    participant_type = 2


class Winners(ListView):
    """
    Display winners list
    """

    model = Contest
    template_name = "contest/winners.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Winners, self).get_context_data(**kwargs)
        context["data"] = Contest.objects.get(id=self.kwargs.get("pk"))
        context["more"] = ContestMore.objects.filter(contest_id=self.kwargs.get("pk"))
        context["participant_works"] = self.get_queryset()
        return context

    def get_queryset(self):
        query1 = ParticipantImages.objects.filter(
            is_active=True, nominees__contest=self.kwargs.get("pk"), position=1
        )
        query2 = ParticipantImages.objects.filter(
            is_active=True, nominees__contest=self.kwargs.get("pk"), position=2
        )
        query3 = ParticipantImages.objects.filter(
            is_active=True, nominees__contest=self.kwargs.get("pk"), position=3
        )
        query = query1 | query2 | query3
        return query


class ParticipantFormView(FormView):
    """
    To be a participant in contest
    """

    template_name = "contest/form.html"
    form_class = ParticipantsForm

    def get_context_data(self, **kwargs):
        context = super(ParticipantFormView, self).get_context_data(**kwargs)
        context["SECRET_KEY"] = CAPTCHA_SECRET_KEY_FOR_HTML
        return context    

    def form_valid(self, form):
        data = form.cleaned_data
        images = []
        captcha_token = self.request.POST.get("g-recaptcha-response")
        cap_url = "https://www.google.com/recaptcha/api/siteverify"
        cap_secret = CAPTCHA_SECRET_KEY
        cap_data = {"secret": cap_secret, "response": captcha_token}
        cap_server_response = requests.post(url=cap_url, data=cap_data)
        cap_json = json.loads(cap_server_response.text)
        if not cap_json["success"]:
            return redirect('.')
        else:
            for i in range(1, 6):
                if data[f"image_{i}"]:
                    obj = {
                        "image": data[f"image_{i}"],
                        "title": data[f"work_{i}"],
                        "tags": data[f"tag_{i}"],
                        "description": data[f"description_{i}"]
                    }
                    images.append(obj)
            contest_id = self.kwargs["pk"]
            nominee = Participant.objects.create(
                contest_id=contest_id,
                author=self.request.user,
                phone_number=PhoneNumber.objects.get(user_id=self.request.user.id),
                participant_type=2,
            )
            for image in images:
                ParticipantImages.objects.create(nominees=nominee, **image)
            return redirect('moderation')


class UserDetailView(ListView):
    model = User
    template_name = 'contest/userDetail.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)
        context["participant_works"] = self.get_queryset()
        context['username'] = User.objects.get(id=self.kwargs.get("pk"))
        return context

    def get_queryset(self):
        query = ParticipantImages.objects.filter(nominees__author_id=self.kwargs.get("pk"), is_active=True)
        return query


@csrf_exempt
def like_nominees(request, pk):
    """
    Ajax request witch count likes on participant work
    """
    if request.user.is_authenticated:
        user = request.user
        if ParticipantUserLike.objects.filter(nominees_id=pk, user_id=user.id).exists():
            return JsonResponse(data={"status": "Вы уже лайкнули"})
        else:
            ParticipantUserLike.objects.create(nominees_id=pk, user_id=user.id)
            return JsonResponse(data={"status": "Вы не можете лайкнуть более одного раза"})
    return JsonResponse(data={"status": "Не авторизованы"})


def signup(request):
    """
    :param request:
    :return: Function based view for Register Form and Email confirmation
    """

    if request.user.is_authenticated:
        return redirect("homepage")
    else:
        form = RegisterForm()
        if request.method == "POST":
            form = RegisterForm(request.POST)
            if form.is_valid():
                user = form.save(commit=False)
                user.is_active = False
                user.save()
                PhoneNumber.objects.create(user_id=user.id, phone_number=form.cleaned_data.get('phone'))
                current_site = get_current_site(request)
                mail_subject = 'Участие в конкурсе от National Geographic Qazaqstan'
                message = render_to_string('contest/acc_active_email.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })
                to_email = form.cleaned_data.get('email')
                email = EmailMessage(
                    mail_subject, message, settings.EMAIL_HOST_USER, [to_email]
                )
                email.send(fail_silently=False)
                return redirect("confirm")
        return render(request, "contest/registration.html", {"form": form})


def login_view(request):
    """
    :param request:
    :return: Function based view for login
    """

    if request.user.is_authenticated:
        return redirect("homepage")
    else:
        form = LoginForm()
        if request.method == "POST":
            form = LoginForm(request.POST)
            username = request.POST.get("username")
            password = request.POST.get("password")
            if form.is_valid():
                user = authenticate(request, username=username, password=password)
                if user is not None:
                    login(request, user)
                    return redirect("homepage")
        context = {"form": form}
        return render(request, "contest/login.html", context)


def logout_view(request):
    """
    :param request:
    :return: Logout view
    """

    logout(request)
    return redirect("homepage")


def activate(request, uidb64, token):
    """
    :param request:
    :param uidb64:
    :param token:
    :return: Function which check confirmation
    """

    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        # return redirect('home')
        return redirect("homepage")

class AdditionalView(TemplateView):
    template_name = "contest/morePage.html"

    def get_context_data(self, **kwargs):
        context = super(AdditionalView, self).get_context_data(**kwargs)
        context["data"] = Contest.objects.get(id=self.kwargs.get("pk"))
        context["more"] = ContestMore.objects.filter(contest_id=self.kwargs.get("pk"))
        context['more_data'] = ContestMore.objects.get(id=self.kwargs.get("np"), contest_id=self.kwargs.get("pk"))
        return context

@user_passes_test(lambda u: u.is_superuser)
def export_excel2(request, pk):
    from excel_response import ExcelResponse
    columns = [['№пп', 'Фотография', 'Имя автора', 'Фамилия автора', 'Электронный адрес участ.', 'Номер телефона', 'Номинация',
               'Название работы',
               'Дата выгрузки', 'Обработан']]
    rows = ParticipantImages.objects.filter(nominees__contest_id=pk).values_list('nominees_id', 'image',
                                    'nominees__author__first_name',
                                    'nominees__author__last_name',
                                    'nominees__author__email',
                                    'nominees__phone_number__phone_number',
                                    'tags__tag', 'title', 'created', 'is_active')
    for row in rows:
        columns.append(row)
    return ExcelResponse(columns, f"Otchet{str(datetime.now())}")



@user_passes_test(lambda u: u.is_superuser)
def export_excel(request, pk):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=Otchet'+str(datetime.now())+'.xls'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Участники')
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour green;'
                             'font: colour black, bold True;')
    columns = ['№пп', 'Фотография', 'Имя автора', 'Фамилия автора', 'Электронный адрес участ.', 'Номер телефона', 'Номинация',
               'Название работы',
               'Дата выгрузки']
    for col in range(len(columns)):
        ws.write(row_num, col, columns[col], font_style)
    font_style = xlwt.XFStyle()
    rows = ParticipantImages.objects.filter(nominees__contest_id=pk).values_list('nominees_id', 'image',
                                                                                'nominees__author__first_name',
                                                                                'nominees__author__last_name',
                                                                                'nominees__author__email',
                                                                                'nominees__phone_number__phone_number',
                                                                                'tags__tag', 'title', 'created')
    for row in rows:
        row = list(row)
        row[1] = f"https://photocontest.kz/media/{row[1]}"
        row_num += 1

        for col in range(len(row)):
            if col == 1:
                ws.write(row_num, col, xlwt.Formula('HYPERLINK("%s", "LINK")' % str(row[col])))
            else:
                ws.write(row_num, col, str(row[col]), font_style)
    wb.save(response)
    return response
