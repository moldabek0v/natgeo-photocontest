from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
import uuid
import os
from ckeditor.fields import RichTextField
from django.utils.safestring import mark_safe


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('uploads/', instance.__class__.__name__, filename)


class Contest(models.Model):
    title = models.CharField(max_length=255, verbose_name="Имя конкурса")
    photo = models.ImageField(
        upload_to=get_file_path, default=None, verbose_name="Фотография конкурса"
    )
    start_date = models.DateField(verbose_name="Время начало конкурса")
    end_date = models.DateField(verbose_name="Время окончания конкурса")
    description = RichTextField(verbose_name="Описание конкурса")

    def __str__(self):
        return f"{self.title}"

    def get_absolute_url(self):
        return f"/inside/{self.id}/"

    def get_excel(self):
        return mark_safe('<a href="%s">Скачать отчет</a>' % f'/export_excel/{self.id}/')

    get_excel.short_description = 'Отчеты'
    get_excel.allow_tags = True

    @property
    def is_active(self):
        return self.start_date <= datetime.now().date() <= self.end_date

    @property
    def coming(self):
        return datetime.now().date() < self.start_date

    class Meta:
        verbose_name = "Конкурс"
        verbose_name_plural = "Конкурсы"
        ordering = ["-start_date", "end_date"]


class Participant(models.Model):
    PARTICIPANT_TYPE = [(1, "Номинанты"), (2, "Фавориты")]

    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="author", verbose_name="Автор"
    )
    phone_number = models.ForeignKey("PhoneNumber", on_delete=models.CASCADE, blank=True, null=True)
    contest = models.ForeignKey(
        Contest,
        on_delete=models.CASCADE,
        related_name="contest",
        verbose_name="Конкурс",
    )
    participant_type = models.IntegerField(
        choices=PARTICIPANT_TYPE, default=1, verbose_name="Типы участников"
    )

    def get_absolute_url(self):
        return f"participant/{self.id}/"

    @property
    def drop(self):
        return "SEPTEMBER"

    class Meta:
        verbose_name = "Участник"
        verbose_name_plural = "Участники"

    @property
    def main_image(self):
        if self.photo.count() > 0:
            return self.photo.first().image.url
        return ""

    def __str__(self):
        return self.author.username


class Tags(models.Model):
    tag = models.CharField(max_length=255, verbose_name="Номинации", null=True)

    def __str__(self):
        return f'{self.tag}'

    class Meta:
        verbose_name = "Номинация"
        verbose_name_plural = "Номинации"


class ContestMore(models.Model):
    contest = models.ForeignKey(Contest, on_delete=models.CASCADE, verbose_name="Конкурс")
    title = models.CharField(max_length=255, verbose_name="Имя дополнительного меню")
    description = RichTextField(verbose_name="Описание")

    def __str__(self):
        return f"{self.title}"

    class Meta:
        verbose_name = "Дополнительная меню"
        verbose_name_plural = "Дополнительные меню"


class ParticipantImages(models.Model):
    POSITION = [(1, "1-е место"), (2, "2-е места"), (3, "3-е места"), (4, None)]
    nominees = models.ForeignKey(
        Participant,
        on_delete=models.CASCADE,
        related_name="photo",
        verbose_name="Работа участника",
    )
    title = models.CharField(max_length=255, verbose_name="Название картины")
    image = models.ImageField(upload_to=get_file_path, verbose_name="картинки")
    is_active = models.BooleanField(verbose_name="Публиковать", default=False)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Добавлено")
    position = models.IntegerField(
        choices=POSITION, default=4, verbose_name="Позиции участников"
    )
    tags = models.ForeignKey(
        Tags, on_delete=models.CASCADE, related_name="tags", null=True, blank=True, verbose_name="Номинация"
    )
    description = models.TextField(verbose_name="Описание", null=True, blank=True, default=None)


    class Meta:
        verbose_name = "Фотография"
        verbose_name_plural = "Работы"

    def __str__(self):
        return str(self.nominees)
    
    def image_tag(self):
        return mark_safe('<img src="%s" width="150" height="150" />' % self.image.url)
    image_tag.short_description = 'Работа'
    image_tag.allow_tags = True


class ParticipantUserLike(models.Model):
    nominees = models.ForeignKey(
        ParticipantImages, on_delete=models.CASCADE, related_name="like"
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="likes")


class Footer(models.Model):
    name = models.CharField(max_length=255, verbose_name="Футер")
    number_for_ordering = models.PositiveIntegerField(default=0, verbose_name='Номер по счету')

    class Meta:
        verbose_name = "Столбец Футера"
        verbose_name_plural = "Столбцы Футера"
        ordering = ['number_for_ordering']

    def __str__(self):
        return f"{self.name}"


class FooterDetail(models.Model):
    name = models.CharField(max_length=255, verbose_name="Детали футера")
    path = models.CharField(max_length=255, verbose_name="URL")
    detail = models.ForeignKey(Footer, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = "Данные столбца Футера"
        verbose_name_plural = "Данные столбцов Футера"


class PhoneNumber(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=255)

    def __str__(self):
        return self.phone_number
