from django.contrib.auth import views as auth_views
from django.contrib.auth.views import LoginView
from django.core.validators import validate_email
from django.urls import path, include
from django.views.generic import TemplateView

from main.views import (
    ContestListView,
    ContestDetailView,
    ContestNomineesView,
    ParticipantView,
    like_nominees,
    ParticipantFavoriteView,
    ParticipantFormView,
    Winners,
    signup,
    activate,
    login_view,
    logout_view, UserDetailView,
    AdditionalView,
    export_excel,
    export_excel2,
)

urlpatterns = [
    path("", ContestListView.as_view(), name="homepage"),
    path("signup/", signup, name="signup"),
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("activate/<uidb64>/<token>", activate, name="activate"),
    path("inside/<int:pk>/", ContestDetailView.as_view(), name="inside"),
    path('user/<int:pk>/', UserDetailView.as_view(), name="users"),
    path(
        "inside/<int:pk>/participants/", ContestNomineesView.as_view(), name="nominee"
    ),
    path(
        "inside/<int:pk>/favorites/", ParticipantFavoriteView.as_view(), name="favorite"
    ),
    path("inside/<int:pk>/winners/", Winners.as_view(), name="winners"),
    path("participant/<int:pk>/", ParticipantView.as_view(), name="nominee_detail"),
    path("ajax_like_nominees/<int:pk>/", like_nominees, name="like_nominee"),
    path("contest/participate/<int:pk>/", ParticipantFormView.as_view(), name="form"),
    path("confirm/", TemplateView.as_view(template_name="contest/confirmEmail.html"), name="confirm"),
    path('success_form/', TemplateView.as_view(template_name="contest/moderation.html"), name="moderation"),
    path('inside/<int:pk>/more/<int:np>/', AdditionalView.as_view(), name="mores"),
    path('export_excel/<int:pk>/', export_excel, name='excel'),
    path('export_excel2/<int:pk>/', export_excel2, name='excel'),
    
    path('reset_password/',
         auth_views.PasswordResetView.as_view(template_name='account/pass_reset.html'),
         name='reset_password'),
    path('reset_password_sent/',
         auth_views.PasswordResetDoneView.as_view(template_name='account/send_email_confirm.html'),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name="account/pass_reset_form.html"),
         name='password_reset_confirm'),
    path('reset_password_complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name='account/pass_reset_done.html'),
         name='password_reset_complete'),
]
