from .models import Participant, Contest, ParticipantImages, ParticipantUserLike, Tags, ContestMore
from django.views.generic import DetailView, ListView


class ContestParticipantMixin(ListView):
    """
    List of Nominees witch can be order by popularity and newest
    """

    model = Contest
    template_name = "contest/nominees.html"
    participant_type = None

    def get_context_data(self, *, object_list=None, **kwargs):
        popular = self.request.GET.get("popular") or False
        context = super(ContestParticipantMixin, self).get_context_data(**kwargs)
        context["participant_type"] = self.participant_type
        print(self.participant_type)
        context["data"] = Contest.objects.get(id=self.kwargs.get("pk"))
        context["more"] = ContestMore.objects.filter(contest_id=self.kwargs.get("pk"))
        context["popular"] = popular
        context["participant_works"] = self.get_queryset(popular=popular)
        context["tags"] = Tags.objects.filter(
            tags__nominees__contest=self.kwargs.get("pk"), tags__is_active=True
        ).distinct("tag")
        return context

    def get_queryset(self, popular=False):
        filter_by_tag = self.request.GET.get("filter")
        if self.participant_type == 1:
            if filter_by_tag is None:
                query = ParticipantImages.objects.filter(
                    is_active=True,
                    nominees__contest=self.kwargs.get("pk"),
                )
            else:
                query = ParticipantImages.objects.filter(
                    is_active=True,
                    nominees__contest=self.kwargs.get("pk"),
                    tags_id=filter_by_tag,
                )
        else:
            if filter_by_tag is None:
                query = ParticipantImages.objects.filter(
                    is_active=True,
                    nominees__participant_type=self.participant_type,
                    nominees__contest=self.kwargs.get("pk"),
                )
            else:
                query = ParticipantImages.objects.filter(
                    is_active=True,
                    nominees__participant_type=self.participant_type,
                    nominees__contest=self.kwargs.get("pk"),
                    tags_id=filter_by_tag,
                )
        return query.order_by("like") if popular else query.order_by("-created")
