from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import Participant, Tags, PhoneNumber
from django import forms
from django.core.exceptions import ValidationError


class ParticipantsForm(forms.Form):
    image_1 = forms.ImageField(label="Работа #1")
    work_1 = forms.CharField(label="Название Работы")
    tag_1 = forms.ModelChoiceField(queryset=Tags.objects.all(), required=False, label="Тэг")
    description_1 = forms.CharField(label="Описание", widget=forms.Textarea, max_length=300)
    image_2 = forms.ImageField(label="Работа #2", required=False)
    work_2 = forms.CharField(label="Название Работы", required=False)
    tag_2 = forms.ModelChoiceField(queryset=Tags.objects.all(), label="Тэг", required=False)
    description_2 = forms.CharField(label="Описание", required=False, widget=forms.Textarea)
    image_3 = forms.ImageField(label="Работа #3", required=False)
    work_3 = forms.CharField(label="Название Работы", required=False)
    tag_3 = forms.ModelChoiceField(queryset=Tags.objects.all(), label="Тэг", required=False)
    description_3 = forms.CharField(label="Описание", required=False, widget=forms.Textarea)
    image_4 = forms.ImageField(label="Работа #4", required=False)
    work_4 = forms.CharField(label="Название Работы", required=False)
    tag_4 = forms.ModelChoiceField(queryset=Tags.objects.all(), label="Тэг", required=False)
    description_4 = forms.CharField(label="Описание", required=False, widget=forms.Textarea)
    image_5 = forms.ImageField(label="Работа #5", required=False)
    work_5 = forms.CharField(label="Название Работы", required=False)
    tag_5 = forms.ModelChoiceField(queryset=Tags.objects.all(), label="Тэг", required=False)
    description_5 = forms.CharField(label="Описание", required=False, widget=forms.Textarea)

    def clean(self):
        cleaned_data = super(ParticipantsForm, self).clean()
        data = self.cleaned_data
        tags = {'tag_1': '', 'tag_2': '', 'tag_3': '', 'tag_4': '', 'tag_5': ''}
        check_work = any([data[f"work_{i}"] for i in range(1, 6)])
        check_description = any([data[f"description_{i}"] for i in range(1, 6)])
        check_image = any([data[f"image_{i}"] for i in range(1, 6)])
        check_tag = any([data[f'tag_{i}'] for i in range(1, 6)])
        if check_tag:
            tags['tag_1'] += data['tag_1'].tag
        if not check_image or not check_work or not check_description:
            self.add_error("work_1", "Заполните хотя бы одну работу полностью")
        if data['image_1'].size > 600000:
            self.add_error('image_1', "Размер файла должен быть не более 600 КБ")
        for i in range(2, 6):
            if data[f"image_{i}"] is not None:
                if data[f'tag_{i}'] is not None:
                    tags[f'tag_{i}'] += data[f'tag_{i}'].tag
                if data[f'image_{i}'].size > 600000:
                    self.add_error(f"image_{i}", "Размер файла должен быть не более 600 КБ")
                if data[f"description_{i}"] == '':
                    self.add_error(f"description_{i}", 'Без описания мы не сможем принять работу:(')
                if data[f"work_{i}"] == '':
                    self.add_error(f"work_{i}", "Без названия мы не сможем принять работу:(")
        tag_list = []
        tag_dict = {}
        for k, v in tags.items():
            if tags[k] != '':
                tag_list.append(tags[k])
        for item in tag_list:
            tag_dict[item] = tag_list.count(item)
        for k, v in tag_dict.items():
            if tag_dict[k] >= 3:
                self.add_error("tag_3", "По условиям конурса вы можете, на одну номинацию только 2 "
                                         "фотографии")
        return cleaned_data


class RegisterForm(UserCreationForm):
    username = forms.CharField(max_length=255, label="Қолданушының аты")
    phone = forms.CharField(max_length=11, widget=forms.NumberInput)

    class Meta:
        model = User
        fields = ["username", "first_name", "last_name", "phone", "email", "password1", "password2"]

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')
        phone = self.cleaned_data.get('phone')
        if PhoneNumber.objects.filter(phone_number=phone):
            self.add_error('phone', "Такой номер уже используется")
        if User.objects.filter(email=email).exists():
            self.add_error('email', 'Такая почта уже зарегистрирована')
        if User.objects.filter(username=username).exists():
            self.add_error('username', "Такой аккаунт уже есть")
        return cleaned_data


class LoginForm(forms.Form):
    username = forms.CharField(max_length=255, label="Имя пользователя")
    password = forms.CharField(
        max_length=255, label="Пароль", widget=forms.PasswordInput()
    )

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        print("check")
        if not User.objects.filter(username=username).exists():
            self.add_error('username', 'Такого аккаунта не существует')
        return cleaned_data

    class Meta:
        model = User
        fields = "__all__"
