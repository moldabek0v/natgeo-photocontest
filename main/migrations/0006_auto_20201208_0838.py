# Generated by Django 2.2.17 on 2020-12-08 02:38

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20201127_1634'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='description_kk',
            field=ckeditor.fields.RichTextField(null=True, verbose_name='Описание конкурса'),
        ),
        migrations.AddField(
            model_name='contest',
            name='description_ru',
            field=ckeditor.fields.RichTextField(null=True, verbose_name='Описание конкурса'),
        ),
        migrations.AddField(
            model_name='contest',
            name='title_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='Имя конкурса'),
        ),
        migrations.AddField(
            model_name='contest',
            name='title_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='Имя конкурса'),
        ),
        migrations.AddField(
            model_name='contestmore',
            name='description_kk',
            field=ckeditor.fields.RichTextField(null=True, verbose_name='Описание'),
        ),
        migrations.AddField(
            model_name='contestmore',
            name='description_ru',
            field=ckeditor.fields.RichTextField(null=True, verbose_name='Описание'),
        ),
        migrations.AddField(
            model_name='contestmore',
            name='title_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='Имя дополнительного меню'),
        ),
        migrations.AddField(
            model_name='contestmore',
            name='title_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='Имя дополнительного меню'),
        ),
        migrations.AddField(
            model_name='footer',
            name='name_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='Футер'),
        ),
        migrations.AddField(
            model_name='footer',
            name='name_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='Футер'),
        ),
        migrations.AddField(
            model_name='footerdetail',
            name='name_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='Детали футера'),
        ),
        migrations.AddField(
            model_name='footerdetail',
            name='name_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='Детали футера'),
        ),
        migrations.AddField(
            model_name='participantimages',
            name='description_kk',
            field=models.TextField(blank=True, default=None, null=True, verbose_name='Описание'),
        ),
        migrations.AddField(
            model_name='participantimages',
            name='description_ru',
            field=models.TextField(blank=True, default=None, null=True, verbose_name='Описание'),
        ),
        migrations.AddField(
            model_name='participantimages',
            name='title_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='Название картины'),
        ),
        migrations.AddField(
            model_name='participantimages',
            name='title_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='Название картины'),
        ),
        migrations.AddField(
            model_name='tags',
            name='tag_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='Номинации'),
        ),
        migrations.AddField(
            model_name='tags',
            name='tag_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='Номинации'),
        ),
    ]
