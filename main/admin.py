from modeltranslation.admin import TranslationAdmin
from django.contrib.admin import TabularInline, register, site
from django.contrib import admin
from main.models import Contest, Participant, ParticipantImages, Tags, FooterDetail, Footer, ContestMore, PhoneNumber


class ParticipantImagesTabularInline(TabularInline):
    model = ParticipantImages


@register(Participant)
class ParticipantAdmin(admin.ModelAdmin):
    inlines = [
        ParticipantImagesTabularInline,
    ]
    list_display = ["author", "contest"]
    list_filter = ["participant_type"]
    exclude = ["participant_type"]

class FooterTabularInline(TabularInline):
    model = FooterDetail


@register(FooterDetail)
class FooterDetailAdmin(TranslationAdmin):
    list_display = ['name']


@register(ParticipantImages)
class ParticipantImageAdmin(TranslationAdmin):
    list_display = ['image_tag', 'nominees', 'created', 'is_active', ]
    readonly_fields = ['image_tag', ]


@register(Contest)
class ContestAdmin(TranslationAdmin):
    list_display = ["title", "get_excel"]
    readonly_fields = ["get_excel"]


@register(Tags)
class TagsAdmin(TranslationAdmin):
    list_display = ('tag', )


@register(Footer)
class FooterAdmin(TranslationAdmin):
    list_display = ('name', )
    inlines = [
        FooterTabularInline,
    ]


@register(ContestMore)
class ContestMoreAdmin(TranslationAdmin):
    list_display = ("contest", "title")


@register(PhoneNumber)
class PhoneNumberAdmin(admin.ModelAdmin):
    list_display = ("user", "phone_number")
    search_fields = ['user__username']


site.site_header = "natgeo-photocopies.kz"
site.site_title = "natgeo-photocontest.kz"
# admin.site.register(PhoneNumber)

