from django.utils.deprecation import MiddlewareMixin

from main.models import FooterDetail


class GetFooterInfo(MiddlewareMixin):

    def process_request(self, request):
        if not hasattr(request, 'footer_detail'):
            footer_detail_list = FooterDetail.objects.all().order_by('detail__ord')
            request.footer_detail_list = footer_detail_list
            print(request)
